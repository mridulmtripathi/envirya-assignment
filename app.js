var serverList = require('./serverList.json');
var comparePriority = require('./utilities');
var httpCall = require('./http');
const { stat } = require('fs');
var successServerList = []; // array initialized to only contain url data which are valid

// Creating function with Promise for accessing if the URL is valid as per criteria
var addServerPromise = function (url, priority) {
  return new Promise(async function (resolve, reject) {
    loadServerData(url).then((status) => {
      if (status && status >= 200 && status <= 299) {
        successServerList.push({
          url: url,
          priority: priority,
        });
      }
      resolve();
    });
  });
};

var loadServerData = (url) => {
  return httpCall(url).then((response) => {
    if (response) {
      return response.status;
    }
    return response;
  });
};

// defining the function 'findServer' to get the list of valid URLs
function findServer(serverList) {
  return new Promise(function (resolve, reject) {
    const promises = [];

    // adding promises for each URL in the promises array
    serverList.forEach((server) => {
      promises.push(addServerPromise(server.url, server.priority));
    });

    // Calling Promises with 'allSettled' so that we get the result once all of the promises complete thier execution
    Promise.allSettled(promises).then(() => {
      if (successServerList.length > 0) {
        successServerList.sort(comparePriority); // sorting the valid list with respect to the priority

        resolve(successServerList[0]);
      }

      reject('No Servers are online');
    });
  });
}

findServer(serverList);

// calling the root function to generate the list of validated URLs
module.exports = findServer;
