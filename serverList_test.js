var serverList = {};

serverList.server_mixed = [
  { url: 'http://doesNotExist.boldtech.co', priority: 1 },
  { url: 'http://boldtech.co', priority: 7 },
  { url: 'http://offline.boldtech.co', priority: 2 },
  { url: 'http://google.com', priority: 4 },
  { url: 'https://angel.co', priority: 0 },
  { url: 'https://classroom.github.com', priority: 8 },
  { url: 'https://www.netlify.com', priority: 9 },
  { url: 'https://gitlab.com', priority: 9 },
  { url: 'http://abc.xyz', priority: 9 },
  { url: 'http://xyz.abc', priority: 9 },
];

serverList.server_only_valid = [
  { url: 'http://google.com', priority: 4 },
  { url: 'https://angel.co', priority: 0 },
  { url: 'https://www.netlify.com', priority: 9 },
  { url: 'https://gitlab.com', priority: 2 },
];

serverList.server_only_invalid = [
  { url: 'http://doesNotExist.boldtech.co', priority: 1 },
  { url: 'https://classroom.github.com', priority: 8 },
  { url: 'http://xyz.abc', priority: 9 },
  { url: 'https://www.msx.cx', priority: 7 },
];

serverList.server_repetitive_urls = [
  { url: 'http://google.com', priority: 1 },
  { url: 'http://google.com', priority: 2 },
  { url: 'http://google.com', priority: 3 },
  { url: 'http://google.com', priority: 4 },
  { url: 'http://google.com', priority: 5 },
  { url: 'http://google.com', priority: 6 },
];

serverList.server_repetitive_priority = [
  { url: 'http://google.com', priority: 4 },
  { url: 'https://netlify.com', priority: 4 },
  { url: 'https://gitlab.com', priority: 4 },
  { url: 'http://abc.xyz', priority: 4 },
  { url: 'https://bing.com', priority: 4 },
  { url: 'https://www.apple.com', priority: 4 },
];

serverList.server_repetitive_urls_and_priority = [
  { url: 'http://google.com', priority: 2 },
  { url: 'http://google.com', priority: 2 },
  { url: 'http://google.com', priority: 2 },
  { url: 'http://google.com', priority: 2 },
  { url: 'http://google.com', priority: 2 },
  { url: 'http://google.com', priority: 2 },
  { url: 'http://google.com', priority: 2 },
  { url: 'http://google.com', priority: 2 },
];

module.exports = serverList;
