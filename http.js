const axios = require('axios');

const fetchData = function (url) {
  return axios
    .get(url, { name: 'activeServer' }, { timeout: 5000 })
    .then((response) => {
      return response;
    })
    .catch(function (error) {
      return error;
    });
};

module.exports = fetchData;
