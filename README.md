Running the application
* Clone the project in local
* Open terminal in the root directory
* Run 'npm install' to download all the required dependencies
* In order to execute the node module run 'npm start'
* To initiate the unit tests run 'npm test' 