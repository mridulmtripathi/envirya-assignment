const { test, expect } = require('@jest/globals');
const axios = require('axios');
// Mock out all top level functions, such as get, put, delete and post:
jest.mock('axios');

const findServer = require('./app');
const serverList = require('./serverList_test');

axios.get.mockImplementation((url) => {
  switch (url) {
    case 'http://doesNotExist.boldtech.co':
      return Promise.resolve({ status: 500, data: {} });

    case 'http://boldtech.co':
      return Promise.resolve({ status: 300, data: {} });

    case 'http://offline.boldtech.co':
      return Promise.resolve({ status: 400, data: {} });

    case 'http://google.com':
      return Promise.resolve({ status: 200, data: {} });

    case 'https://classroom.github.com':
      return Promise.resolve({ status: 403, data: {} });

    case 'https://www.netlify.com':
      return Promise.resolve({ status: 200, data: {} });

    case 'https://gitlab.com':
      return Promise.resolve({ status: 206, data: {} });

    case 'http://abc.xyz':
      return Promise.resolve({ status: 200, data: {} });

    case 'http://xyz.abc':
      return Promise.resolve({ status: 400, data: {} });

    case 'https://bing.com':
      return Promise.resolve({ status: 205, data: {} });

    case 'https://www.apple.com':
      return Promise.resolve({ status: 200, data: {} });

    case 'https://www.youtube.com':
      return Promise.resolve({ status: 200, data: {} });

    case 'https://www.cloudflare.com':
      return Promise.resolve({ status: 200, data: {} });

    case 'https://www.convertonline.io':
      return Promise.resolve({ status: 201, data: {} });

    case 'https://www.npmjs.com':
      return Promise.resolve({ status: 202, data: {} });

    case 'https://www.msn.com':
      return Promise.resolve({ status: 200, data: {} });

    case 'https://www.msn.co.in':
      return Promise.resolve({ status: 200, data: {} });

    case 'https://www.msx.cx':
      return Promise.resolve({ status: 500, data: {} });

    default:
      return Promise.resolve({ status: 500, data: {} });
  }
});

test('testing for mixed type of values, least priority is 4', (done) => {
  findServer(serverList.server_mixed).then((response) => {
    expect(response).toStrictEqual({ priority: 4, url: 'http://google.com' });
    done();
  });
});

test('testing for all valid urls, least priority is 0', (done) => {
  findServer(serverList.server_only_valid).then((response) => {
    expect(response).toStrictEqual({ priority: 2, url: 'https://gitlab.com' });
    done();
  });
});

test('testing for all invalid urls', (done) => {
  findServer(serverList.server_only_invalid).then((response) => {
    expect(response).toStrictEqual({ priority: 2, url: 'https://gitlab.com' });
    done();
  });
});

test('testing for input list with repetitive urls', (done) => {
  findServer(serverList.server_repetitive_urls).then((response) => {
    expect(response).toStrictEqual({ priority: 1, url: 'http://google.com' });
    done();
  });
});

test('testing for input list with repetitive priority', (done) => {
  findServer(serverList.server_repetitive_priority).then((response) => {
    expect(response).toStrictEqual({ priority: 1, url: 'http://google.com' });
    done();
  });
});

test('testing for input list with repetitive url and  priority', (done) => {
  findServer(serverList.server_repetitive_urls_and_priority).then(
    (response) => {
      expect(response).toStrictEqual({ priority: 1, url: 'http://google.com' });
      done();
    }
  );
});
